#!/bin/bash
#The purpose of this file is to document the steps followed to install the RTL8812AU drivers for NETGEAR AC6100 WiFi Dongle.

# Run as root

# Once the script has finished, unplug and replug the adapter.

# If needed, restart NetworkManager:
# $ sudo systemctl restart NetworkManager

cd ~
apt install python-is-python3 -y
git clone -b v5.6.4.2 https://github.com/aircrack-ng/rtl8812au.git
cd rtl*
sudo apt-get install -y libssl-dev build-essential 
cd /usr/src/linux-headers-$(uname -r)
sudo make scripts
cd ~/rtl*
sed -i 's/CONFIG_PLATFORM_I386_PC = y/CONFIG_PLATFORM_I386_PC = n/g' Makefile
sed -i 's/CONFIG_PLATFORM_ARM64_RPI = n/CONFIG_PLATFORM_ARM64_RPI = y/g' Makefile
export ARCH=arm64
sed -i 's/^MAKE="/MAKE="ARCH=arm64\ /' dkms.conf
make
make install
systemctl restart NetworkManager
