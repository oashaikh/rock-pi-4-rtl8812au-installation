# This shows how to generate an access point that shares an internet connection with another interface

#---
# Install iptables:
apt install iptables -y
#---


#---
# Install hostapd:
apt install hostapd -y
#---


#---
# Install dnsmasq:
apt install dnsmasq -y
#---


#---
systemctl disable dnsmasq
systemctl stop dnsmasq
#---


#---
# Edit/create /etc/NetworkManager/NetworkManager.conf and add the following:
#[main]
#dns=dnsmasq

sed -i '/^plugins=.*/a dns=dnsmasq' /etc/NetworkManager/NetworkManager.conf
#---

#---
# Add the hotsot connection:

#nmcli con add type wifi ifname wlan0 mode ap con-name <name> ssid <ssid>
#nmcli con modify <name> 802-11-wireless.band <band>
#nmcli con modify <name> 802-11-wireless.channel <channel>
#nmcli con modify <name> 802-11-wireless-security.key-mgmt <key-mgmt>
#nmcli con modify <name> 802-11-wireless-security.proto <proto>
#nmcli con modify <name> 802-11-wireless-security.group <group>
#nmcli con modify <name> 802-11-wireless-security.pairwise <pairwise>
#nmcli con modify <name> 802-11-wireless-security.psk <passphrase>
#nmcli con modify <name> ipv4.method shared
#nmcli con up <name>

# For example:

nmcli con add type wifi ifname wlxc89e43930d60 mode ap con-name rock ssid rock
nmcli con modify rock 802-11-wireless.band bg
nmcli con modify rock 802-11-wireless.channel 1
nmcli con modify rock 802-11-wireless-security.key-mgmt wpa-psk
nmcli con modify rock 802-11-wireless-security.proto rsn
nmcli con modify rock 802-11-wireless-security.group ccmp
nmcli con modify rock 802-11-wireless-security.pairwise ccmp
nmcli con modify rock 802-11-wireless-security.psk "1234567890"
nmcli con modify rock ipv4.method shared
nmcli con up rock
systemctl restart NetworkManager
#---


#---
# Open /etc/sysctl.conf and add:
# net.ipv4.ip_forward=1
echo 1 > /proc/sys/net/ipv4/ip_forward

# Then reload the file

sed "s/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g" /etc/sysctl.conf
sysctl -p /etc/sysctl.conf
#---


#---
# Open /etc/rc.local and add the following to the end:

# Replace online_device with the name of the network device that provides the internet connection
# Replace lan_device with the name of the network device that connects to the LAN

sed -i /etc/rc.local -e '/^exit/ i \
iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE \
iptables -A FORWARD -i wlan0 -o wlxc89e43930d60 -m state --state RELATED,ESTABLISHED -j ACCEPT \
iptables -A FORWARD -i wlxc89e43930d60 -o wlan0 -j ACCEPT'




# For example:

#iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
#sudo iptables -A FORWARD -i wlan0 -o wlxc89e43930d60 -m state --state RELATED,ESTABLISHED -j ACCEPT
#iptables -A FORWARD -i wlxc89e43930d60 -o wlan0 -j ACCEPT

# Also add:

service hostapd stop
#---


#---

#---

